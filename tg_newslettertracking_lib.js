
tgNamespace.tgContainer.registerInnerGate("newsletter-tracking", function(oWrapper, oLocalConfData)
{

    this.oWrapper = oWrapper;
    this.oLocalConfData = oLocalConfData;
    this.currentSite;


    this._construct = function()
    {
        if(this.oWrapper.debug("newsletter-tracking","_construct"))debugger;

        this.oWrapper.registerInnerGateFunctions("newsletter-tracking","form_submit",this.trackFormSubmit);

    }


    //Hängt sich an den Form-Submit Funktion des Formular-Tracking und sendet einen Event-Befehl an UA-Gate

    this.trackFormSubmit = function (oArgs) {
        if (this.oWrapper.debug("newsletter-tracking", "trackFormSubmit")) debugger;

        this.currentSite = location.href;
        var newsletterCheckbox;

        if (this.oLocalConfData.link.test(this.currentSite) === true) {

            newsletterCheckbox = $('input[id=info-newsLetter]');

            if (newsletterCheckbox !== "undefined" && newsletterCheckbox.attr('checked') === "checked") {
                this.oWrapper.exeFunction({
                    track: "event",
                    group: "Kundenkarte",
                    name: "newsletter",
                    value: 1
                });
            }else{
                this.oWrapper.exeFunction({
                    track: "event",
                    group: "Kundenkarte",
                    name: "newsletter",
                    value: 0
                });
            }





        }
    }


});

